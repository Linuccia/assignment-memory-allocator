#include "tests.h"

void normal_memalloc_test() {
   printf("--------NORMAL MEMALLOC TEST--------\n");
   void *content = _malloc(GENERAL_SIZE);
   debug_heap(stdout, HEAP_START);
   _free(content);
   printf("--------NORMAL MEMALLOC TEST END--------\n");
}

void free_block_test() {
   printf("\n--------FREE ONE BLOCK TEST--------\n");
   void *cont1 = _malloc(GENERAL_SIZE - 256);
   void *cont2 = _malloc(GENERAL_SIZE - 128);
   void *cont3 = _malloc(GENERAL_SIZE - 384);
   void *cont4 = _malloc(GENERAL_SIZE - 500);
   debug_heap(stdout, HEAP_START);
   _free(cont2);
   debug_heap(stdout, HEAP_START);
   _free(cont1);
   _free(cont3);
   _free(cont4);
   printf("--------FREE ONE BLOCK TEST END--------\n");
}

void free_two_blocks_test() {
   printf("\n--------FREE TWO BLOCKS TEST--------\n");
   void *cont1 = _malloc(GENERAL_SIZE - 256);
   void *cont2 = _malloc(GENERAL_SIZE - 128);
   void *cont3 = _malloc(GENERAL_SIZE - 384);
   void *cont4 = _malloc(GENERAL_SIZE - 500);
   debug_heap(stdout, HEAP_START);
   _free(cont2);
   _free(cont4);
   debug_heap(stdout, HEAP_START);
   _free(cont1);
   _free(cont3);
   printf("--------FREE TWO BLOCKS TEST END--------\n");
}

void normal_grow_heap_test() {
   printf("\n--------NORMAL GROW HEAP TEST--------\n");
   void *cont1 = _malloc(GENERAL_SIZE - 256);
   void *cont2 = _malloc(GENERAL_SIZE);
   void *cont3 = _malloc(GENERAL_SIZE);
   debug_heap(stdout, HEAP_START);
   _free(cont2);
   _free(cont3);
   _free(cont1);
   printf("--------NORMAL GROW HEAP TEST END--------\n");
}

void another_addr_grow_heap_test() {
   printf("\n--------ANOTHER ADDRESS FOR NEW REGION TEST--------\n");
   void *cont1 = _malloc(HEAP_SIZE * 2);
   void *cont2 = _malloc(HEAP_SIZE);
   debug_heap(stdout, HEAP_START);
   _free(cont1);
   _free(cont2);
   printf("--------ANOTHER ADDRESS FOR NEW REGION TEST END--------\n");
}
