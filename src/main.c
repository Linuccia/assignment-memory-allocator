#include "tests.h"

int main() {
   heap_init(HEAP_SIZE);
   
   normal_memalloc_test();
   free_block_test();
   free_two_blocks_test();
   normal_grow_heap_test();
   another_addr_grow_heap_test();
   
   return 0;
}
