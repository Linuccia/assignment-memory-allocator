#include "mem.h"
#include "mem_internals.h"

#include <stdio.h>

#define HEAP_SIZE 1024
#define GENERAL_SIZE 512


void normal_memalloc_test();

void free_block_test();

void free_two_blocks_test();

void normal_grow_heap_test();

void another_addr_grow_heap_test();
